![Cosmos Logo](https://github.com/nodersteam/picture/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202023-07-19%20105624.png?raw=true)

# Cosmos Network Public Endpoints

## Description

This repository contains public API and RPC endpoints for numerous projects developed on Cosmos SDK. It is a valuable resource for developers looking to find public endpoints for the majority of networks within the Cosmos ecosystem. The repository is automatically updated every 1 hour to always provide up-to-date information.

## Features

- **Automatic Updates**: The list of public endpoints is automatically updated every 1 hour.
- **Wide Selection**: Covers the majority of networks within the Cosmos ecosystem.
- **Easy Access**: Direct links to API and RPC endpoints, convenient for developers.

## How to Use

<details>
  <summary>Mainnets</summary>
  
  Simply browse the mainnets section to find the public endpoints you need for main networks.

<!-- START_MAINNET -->
<details>
<summary>AssetMantle</summary>

- Moniker: **yieldmos-mantle-1**
- Chain ID: ****
- Latest block: **8116178**
- RPC: **65.108.235.36:18657**
- TxIndex: **on**

---

- Moniker: **yieldmos-mantle-1**
- Chain ID: ****
- Latest block: **8116178**
- RPC: **65.108.235.36:18657**
- TxIndex: **on**

---

- Moniker: **2xStake.com**
- Chain ID: ****
- Latest block: **8116178**
- RPC: **65.108.135.212:26657**
- TxIndex: **on**
- API: **65.108.135.212:1317**

---

- Moniker: **PBS**
- Chain ID: ****
- Latest block: **8116178**
- RPC: **62.171.182.242:23657**
- TxIndex: **on**
- API: **62.171.182.242:1313**

---

- Moniker: **ECO Stake**
- Chain ID: ****
- Latest block: **8116178**
- RPC: **65.108.137.22:26657**
- TxIndex: **on**
- API: **65.108.137.22:1317**

---

</details>

<details>
<summary>Aura</summary>

- Moniker: **palamar-archive-node**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **148.251.88.145:10457**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **65.108.141.109:54657**
- TxIndex: **on**
- API: **65.108.141.109:1317**

---

- Moniker: **vidulum.app**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **208.77.197.83:27657**
- TxIndex: **on**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **65.109.93.152:34657**
- TxIndex: **on**

---

- Moniker: **Staketab-snap**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **65.108.195.29:51657**
- TxIndex: **off**
- API: **65.108.195.29:1318**

---

- Moniker: **ramuchi.tech**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **142.132.202.86:30001**
- TxIndex: **on**
- API: **142.132.202.86:1322**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **3307255**
- RPC: **174.138.180.190:60757**
- TxIndex: **on**
- API: **174.138.180.190:1317**

---

</details>

<details>
<summary>EVMOS</summary>

- Moniker: **bricks_evmos_2**
- Chain ID: ****
- Latest block: **16659433**
- RPC: **65.109.84.24:26657**
- TxIndex: **on**
- API: **65.109.84.24:1317**

---

- Moniker: **silent**
- Chain ID: ****
- Latest block: **16659433**
- RPC: **65.108.141.109:7657**
- TxIndex: **on**
- API: **65.108.141.109:1317**

---

- Moniker: **BRAND-evmos-relayer**
- Chain ID: ****
- Latest block: **16659433**
- RPC: **213.239.213.142:13457**
- TxIndex: **on**

---

- Moniker: **abc**
- Chain ID: ****
- Latest block: **16659433**
- RPC: **159.89.80.121:26657**
- TxIndex: **off**

---

- Moniker: **rpc-evmos-mainnet**
- Chain ID: ****
- Latest block: **16659433**
- RPC: **5.9.107.249:46657**
- TxIndex: **on**
- API: **5.9.107.249:1317**

---

- Moniker: **mefn1**
- Chain ID: ****
- Latest block: **16659433**
- RPC: **62.171.184.44:26657**
- TxIndex: **on**
- API: **62.171.184.44:1317**

---

</details>

<details>
<summary>Kujira</summary>

- Moniker: **Sapient Nodes**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **57.128.20.147:30257**
- TxIndex: **off**

---

- Moniker: **StakeLab**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **65.108.106.156:26677**
- TxIndex: **off**
- API: **65.108.106.156:1317**

---

- Moniker: **bricks**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **65.109.80.92:26657**
- TxIndex: **on**

---

- Moniker: **BRAND-kujira-relayer**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **213.239.213.142:11857**
- TxIndex: **on**

---

- Moniker: **bp-kuji-node**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **168.119.15.94:26657**
- TxIndex: **on**

---

- Moniker: **rpc**
- Chain ID: ****
- Latest block: **14990502**
- RPC: **173.212.247.202:26657**
- TxIndex: **on**

---

- Moniker: **rpc**
- Chain ID: ****
- Latest block: **14990502**
- RPC: **173.212.247.202:26657**
- TxIndex: **on**

---

- Moniker: **lenon**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **81.45.139.190:26657**
- TxIndex: **on**

---

- Moniker: **kujira-indexer-2**
- Chain ID: ****
- Latest block: **14990500**
- RPC: **54.154.200.79:26657**
- TxIndex: **on**

---

- Moniker: **0e37d59b70349e559fef74ed668d07a4**
- Chain ID: ****
- Latest block: **14990503**
- RPC: **142.132.248.34:11857**
- TxIndex: **on**

---

</details>

<details>
<summary>Cryptoorg</summary>

- Moniker: **yieldmos**
- Chain ID: ****
- Latest block: **14147583**
- RPC: **65.109.35.50:12657**
- TxIndex: **on**

---

- Moniker: **yieldmos**
- Chain ID: ****
- Latest block: **14147583**
- RPC: **65.109.35.50:12657**
- TxIndex: **on**

---

- Moniker: **BRAND-cryptocom-relayer**
- Chain ID: ****
- Latest block: **14147583**
- RPC: **5.9.99.172:20257**
- TxIndex: **on**

---

- Moniker: **scooby3**
- Chain ID: ****
- Latest block: **14147583**
- RPC: **75.119.135.156:26657**
- TxIndex: **on**

---

- Moniker: **Stakely**
- Chain ID: ****
- Latest block: **14147583**
- RPC: **65.108.142.81:26671**
- TxIndex: **on**
- API: **65.108.142.81:1321**

---

- Moniker: **UbikCapital**
- Chain ID: ****
- Latest block: **14147583**
- RPC: **161.97.115.61:26657**
- TxIndex: **off**

---

</details>

<details>
<summary>Injective</summary>

- Moniker: **BRAND-injective-relayer**
- Chain ID: ****
- Latest block: **49024007**
- RPC: **85.10.197.58:14357**
- TxIndex: **on**

---

- Moniker: **BRAND-injective-relayer**
- Chain ID: ****
- Latest block: **49024007**
- RPC: **85.10.197.58:14357**
- TxIndex: **on**

---

- Moniker: **BRAND-injective-relayer**
- Chain ID: ****
- Latest block: **49024007**
- RPC: **85.10.197.58:14357**
- TxIndex: **on**

---

- Moniker: **p2p-injective-2**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **43.157.62.64:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **49023843**
- RPC: **176.9.147.146:36657**
- TxIndex: **on**
- API: **176.9.147.146:1317**

---

- Moniker: **mainnet-staging-archival-node-0**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **162.55.103.170:26657**
- TxIndex: **on**

---

- Moniker: **bjwe**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **3.73.138.105:26657**
- TxIndex: **on**
- API: **3.73.138.105:1317**

---

- Moniker: **mainnet-products-0**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **15.235.87.154:26657**
- TxIndex: **on**

---

- Moniker: **tienthuattoan**
- Chain ID: ****
- Latest block: **49019195**
- RPC: **209.182.237.121:26657**
- TxIndex: **on**

---

- Moniker: **mainnet-products-1**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **57.128.74.138:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **15.204.206.127:26657**
- TxIndex: **on**

---

- Moniker: **mainnet-node-2**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **15.204.208.167:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **15.235.86.222:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **148.113.153.117:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **148.113.153.117:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **41637147**
- RPC: **23.88.5.151:26657**
- TxIndex: **on**

---

- Moniker: **mainnet-node-3**
- Chain ID: ****
- Latest block: **49024008**
- RPC: **51.81.221.159:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Jackal</summary>

- Moniker: **nkbblocks**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.109.61.114:37657**
- TxIndex: **on**

---

- Moniker: **nkbblocks**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.21.139.150:37657**
- TxIndex: **on**

---

- Moniker: **nkbblocks**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.109.116.57:13757**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.108.141.109:18657**
- TxIndex: **on**
- API: **65.108.141.109:1317**

---

- Moniker: **ams**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.108.44.149:23657**
- TxIndex: **on**

---

- Moniker: **Vagif**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.109.116.50:27657**
- TxIndex: **on**

---

- Moniker: **Vagif**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **94.130.137.122:33657**
- TxIndex: **off**

---

- Moniker: **STAVR-RPC**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **88.99.164.158:11127**
- TxIndex: **on**

---

- Moniker: **vidulum.app**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **208.77.197.83:28657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **65.108.75.107:18657**
- TxIndex: **on**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **174.138.180.190:60857**
- TxIndex: **on**
- API: **174.138.180.190:1327**

---

- Moniker: **jackal-archive**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **167.142.158.242:36657**
- TxIndex: **on**

---

- Moniker: **jackal-archive**
- Chain ID: ****
- Latest block: **4906685**
- RPC: **167.142.158.242:36657**
- TxIndex: **on**

---

</details>

<details>
<summary>Nois</summary>

- Moniker: **L0vd.com | RPC**
- Chain ID: ****
- Latest block: **7615707**
- RPC: **65.109.33.48:13657**
- TxIndex: **on**

---

- Moniker: **STAVR-Service**
- Chain ID: ****
- Latest block: **7615707**
- RPC: **88.99.164.158:40137**
- TxIndex: **on**

---

- Moniker: **STAVR**
- Chain ID: ****
- Latest block: **7615707**
- RPC: **65.109.92.240:40137**
- TxIndex: **on**
- API: **65.109.92.240:1317**

---

- Moniker: **terlia**
- Chain ID: ****
- Latest block: **7615707**
- RPC: **88.198.18.88:32657**
- TxIndex: **on**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **7615707**
- RPC: **174.138.180.190:61457**
- TxIndex: **on**
- API: **174.138.180.190:1327**

---

</details>

<details>
<summary>Teritori</summary>

- Moniker: **node**
- Chain ID: ****
- Latest block: **5739065**
- RPC: **65.108.141.109:15657**
- TxIndex: **on**
- API: **65.108.141.109:1317**

---

- Moniker: **Hermes**
- Chain ID: ****
- Latest block: **5739065**
- RPC: **65.108.70.119:27657**
- TxIndex: **on**

---

- Moniker: **cyberG**
- Chain ID: ****
- Latest block: **5739065**
- RPC: **141.95.65.26:27737**
- TxIndex: **off**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **5739065**
- RPC: **65.108.75.107:15657**
- TxIndex: **on**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **5739065**
- RPC: **174.138.180.190:36657**
- TxIndex: **on**
- API: **174.138.180.190:1327**

---

</details>

<details>
<summary>Agoric</summary>

- Moniker: **PDP_Validator_RPC**
- Chain ID: ****
- Latest block: **12177670**
- RPC: **95.216.5.101:26657**
- TxIndex: **off**

---

- Moniker: **Vagif**
- Chain ID: ****
- Latest block: **10253149**
- RPC: **65.109.116.50:34657**
- TxIndex: **on**

---

- Moniker: **yieldmos-three**
- Chain ID: ****
- Latest block: **12177670**
- RPC: **65.109.35.50:20657**
- TxIndex: **on**

---

- Moniker: **BRAND-agoric-relayer**
- Chain ID: ****
- Latest block: **12177670**
- RPC: **213.239.213.142:14457**
- TxIndex: **on**

---

- Moniker: **bouncy_ball**
- Chain ID: ****
- Latest block: **12177670**
- RPC: **141.94.254.181:42257**
- TxIndex: **off**

---

- Moniker: **Sentry**
- Chain ID: ****
- Latest block: **12177670**
- RPC: **46.166.143.91:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Migaloo</summary>

- Moniker: **USArmy**
- Chain ID: ****
- Latest block: **3779946**
- RPC: **51.89.155.2:23657**
- TxIndex: **on**
- API: **51.89.155.2:1317**

---

- Moniker: **ww-archive**
- Chain ID: ****
- Latest block: **3779946**
- RPC: **23.227.185.210:26657**
- TxIndex: **on**
- API: **23.227.185.210:1317**

---

- Moniker: **wpsprim**
- Chain ID: ****
- Latest block: **3779946**
- RPC: **116.202.143.93:26657**
- TxIndex: **on**
- API: **116.202.143.93:1317**

---

</details>

<details>
<summary>Neutron</summary>

- Moniker: **harry-smith**
- Chain ID: ****
- Latest block: **3913162**
- RPC: **5.9.87.216:39957**
- TxIndex: **on**
- API: **5.9.87.216:1311**

---

- Moniker: **hetzner-node-2**
- Chain ID: ****
- Latest block: **3913162**
- RPC: **65.109.34.57:26657**
- TxIndex: **on**

---

- Moniker: **test**
- Chain ID: ****
- Latest block: **3913162**
- RPC: **86.111.48.144:26657**
- TxIndex: **on**
- API: **86.111.48.144:1317**

---

- Moniker: **neutron-consumer-0**
- Chain ID: ****
- Latest block: **3913162**
- RPC: **34.80.117.56:26657**
- TxIndex: **on**

---

- Moniker: **moniker**
- Chain ID: ****
- Latest block: **3913162**
- RPC: **165.22.104.209:26657**
- TxIndex: **on**
- API: **165.22.104.209:1317**

---

- Moniker: **moniker**
- Chain ID: ****
- Latest block: **3913162**
- RPC: **165.22.106.109:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Nolus</summary>

- Moniker: **RAMZES**
- Chain ID: ****
- Latest block: **2283249**
- RPC: **65.108.199.120:35457**
- TxIndex: **on**
- API: **65.108.199.120:1317**

---

- Moniker: **BRAND-nolus-relayer**
- Chain ID: ****
- Latest block: **2283249**
- RPC: **5.9.99.172:19757**
- TxIndex: **on**

---

</details>

<details>
<summary>Bitsong</summary>

- Moniker: **RAMZES**
- Chain ID: ****
- Latest block: **13770234**
- RPC: **65.108.199.120:26657**
- TxIndex: **on**
- API: **65.108.199.120:1317**

---

</details>

<details>
<summary>ComposableFinance</summary>

- Moniker: **L0vd.com | RPC**
- Chain ID: ****
- Latest block: **2088116**
- RPC: **65.109.33.48:23657**
- TxIndex: **on**

---

- Moniker: **Kyn**
- Chain ID: ****
- Latest block: **1771900**
- RPC: **5.9.61.78:14657**
- TxIndex: **on**

---

- Moniker: **Stakewolle**
- Chain ID: ****
- Latest block: **2088116**
- RPC: **148.113.16.109:16657**
- TxIndex: **on**

---

- Moniker: **orahapris**
- Chain ID: ****
- Latest block: **2088116**
- RPC: **95.216.4.183:15657**
- TxIndex: **on**

---

- Moniker: **serana**
- Chain ID: ****
- Latest block: **2088116**
- RPC: **88.198.18.88:40657**
- TxIndex: **on**

---

- Moniker: **kjgno2uht93**
- Chain ID: ****
- Latest block: **2088116**
- RPC: **93.159.130.4:36657**
- TxIndex: **on**

---

</details>

<details>
<summary>Gitopia</summary>

- Moniker: **L0vd.com | RPC**
- Chain ID: ****
- Latest block: **7952243**
- RPC: **65.109.33.48:22657**
- TxIndex: **on**

---

- Moniker: **STAVR-Service**
- Chain ID: ****
- Latest block: **7952243**
- RPC: **65.108.230.113:51057**
- TxIndex: **on**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **7952243**
- RPC: **174.138.180.190:46657**
- TxIndex: **on**
- API: **174.138.180.190:1327**

---

</details>

<details>
<summary>Kichain</summary>

- Moniker: **node**
- Chain ID: ****
- Latest block: **17615483**
- RPC: **85.10.193.142:26677**
- TxIndex: **on**

---

- Moniker: **AviaDoc_by_AVIAONE**
- Chain ID: ****
- Latest block: **17608164**
- RPC: **194.163.131.83:26677**
- TxIndex: **on**

---

- Moniker: **moonboom**
- Chain ID: ****
- Latest block: **17615483**
- RPC: **109.195.84.200:26657**
- TxIndex: **off**

---

</details>

<details>
<summary>Archway</summary>

- Moniker: **L0vd.com | RPC**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **65.109.33.48:26657**
- TxIndex: **on**

---

- Moniker: **Validatrium-rpc**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **135.181.58.28:27457**
- TxIndex: **on**
- API: **135.181.58.28:1317**

---

- Moniker: **obl-rpc**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **51.89.16.119:26657**
- TxIndex: **on**

---

- Moniker: **ST-Server**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **65.108.75.174:44657**
- TxIndex: **on**

---

- Moniker: **NODERSTEAMSERVICE**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **135.181.192.16:26657**
- TxIndex: **on**
- API: **135.181.192.16:1317**

---

- Moniker: **VitaValeriya**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **193.34.212.220:16657**
- TxIndex: **off**

---

- Moniker: **arcareade**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **5.9.23.47:26657**
- TxIndex: **on**
- API: **5.9.23.47:1317**

---

- Moniker: **cryptech-rpc**
- Chain ID: ****
- Latest block: **1624051**
- RPC: **185.144.99.15:26657**
- TxIndex: **on**
- API: **185.144.99.15:1317**

---

- Moniker: **phx01-archway-node01**
- Chain ID: ****
- Latest block: **1624052**
- RPC: **66.85.142.148:26657**
- TxIndex: **on**
- API: **66.85.142.148:1317**

---

- Moniker: **phx02-archway-node02**
- Chain ID: ****
- Latest block: **1624052**
- RPC: **66.85.142.149:26657**
- TxIndex: **on**
- API: **66.85.142.149:1317**

---

- Moniker: **arcallowance1kr**
- Chain ID: ****
- Latest block: **1624052**
- RPC: **125.131.181.23:26657**
- TxIndex: **on**
- API: **125.131.181.23:1317**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **1624049**
- RPC: **174.138.180.190:56657**
- TxIndex: **on**
- API: **174.138.180.190:1317**

---

</details>

<details>
<summary>Empower</summary>

- Moniker: **Validatrium-rpc**
- Chain ID: ****
- Latest block: **1808203**
- RPC: **135.181.58.28:22357**
- TxIndex: **on**
- API: **135.181.58.28:1317**

---

- Moniker: **yldmsempower**
- Chain ID: ****
- Latest block: **1808203**
- RPC: **142.132.157.153:17457**
- TxIndex: **on**

---

- Moniker: **STAVR-Service**
- Chain ID: ****
- Latest block: **0**
- RPC: **65.108.230.113:22057**
- TxIndex: **on**

---

- Moniker: **ams-rpc**
- Chain ID: ****
- Latest block: **1808203**
- RPC: **161.97.82.203:31657**
- TxIndex: **on**
- API: **161.97.82.203:1319**

---

- Moniker: **BonyNode**
- Chain ID: ****
- Latest block: **1808203**
- RPC: **185.188.249.46:16657**
- TxIndex: **off**

---

- Moniker: **STAVR-Service**
- Chain ID: ****
- Latest block: **0**
- RPC: **65.108.230.113:22057**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **1808203**
- RPC: **62.210.173.13:26657**
- TxIndex: **on**
- API: **62.210.173.13:1317**

---

</details>

<details>
<summary>Juno</summary>

- Moniker: **BRAND-juno-relayer**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **213.239.213.142:12657**
- TxIndex: **on**

---

- Moniker: **dimi**
- Chain ID: ****
- Latest block: **2578097**
- RPC: **78.46.88.98:26657**
- TxIndex: **on**

---

- Moniker: **STAVR-Service**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **88.99.164.158:1067**
- TxIndex: **on**

---

- Moniker: **rpc-8**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **141.94.195.104:26657**
- TxIndex: **on**

---

- Moniker: **rpc-8**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **141.94.195.104:26657**
- TxIndex: **on**

---

- Moniker: **rpc-8**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **141.94.195.104:26657**
- TxIndex: **on**

---

- Moniker: **rpc-8**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **141.94.195.104:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **66.172.36.139:11657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10078449**
- RPC: **66.172.36.140:11657**
- TxIndex: **on**

---

- Moniker: **Stake&Relax-juno-main**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **194.163.172.115:12657**
- TxIndex: **on**

---

- Moniker: **nRide.com Validator**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **212.227.160.56:26657**
- TxIndex: **on**

---

- Moniker: **rpc-3**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **135.181.1.44:26657**
- TxIndex: **on**

---

- Moniker: **moneymoney**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **142.132.248.214:52257**
- TxIndex: **on**
- API: **142.132.248.214:1319**

---

- Moniker: **ZenChainLabs-RPC**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **135.181.5.176:26657**
- TxIndex: **on**

---

- Moniker: **myrpc**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **176.9.139.74:36657**
- TxIndex: **on**
- API: **176.9.139.74:1314**

---

- Moniker: **moneymoney**
- Chain ID: ****
- Latest block: **11023085**
- RPC: **198.244.229.100:52257**
- TxIndex: **off**

---

</details>

<details>
<summary>Meme</summary>

- Moniker: **yieldmos-meme**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **65.109.35.50:27657**
- TxIndex: **on**

---

- Moniker: **yieldmos-meme**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **65.109.35.50:27657**
- TxIndex: **on**

---

- Moniker: **entropic.nodes**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **173.212.220.98:26657**
- TxIndex: **on**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **65.109.28.177:26737**
- TxIndex: **off**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **65.109.28.177:26737**
- TxIndex: **off**

---

- Moniker: **rpc6**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **103.19.25.141:26657**
- TxIndex: **on**
- API: **103.19.25.141:1317**

---

- Moniker: **MEMEFoundation-hk**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **103.19.25.140:26657**
- TxIndex: **on**
- API: **103.19.25.140:1317**

---

- Moniker: **rpc5**
- Chain ID: ****
- Latest block: **8206235**
- RPC: **165.140.242.34:26657**
- TxIndex: **on**
- API: **165.140.242.34:1317**

---

</details>

<details>
<summary>Sommelier</summary>

- Moniker: **BRAND-sommelier-relayer**
- Chain ID: ****
- Latest block: **11395397**
- RPC: **5.9.99.172:14157**
- TxIndex: **on**

---

- Moniker: **BRAND-sommelier-relayer**
- Chain ID: ****
- Latest block: **11395397**
- RPC: **5.9.99.172:14157**
- TxIndex: **on**

---

- Moniker: **BRAND-sommelier-relayer**
- Chain ID: ****
- Latest block: **11395397**
- RPC: **5.9.99.172:14157**
- TxIndex: **on**

---

- Moniker: **Stakewolle**
- Chain ID: ****
- Latest block: **11395397**
- RPC: **148.113.6.121:34657**
- TxIndex: **off**

---

</details>

<details>
<summary>GravityBridge</summary>

- Moniker: **3ventures.io | autocompound via reStake**
- Chain ID: ****
- Latest block: **8910027**
- RPC: **173.249.41.78:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **5.9.106.214:26657**
- TxIndex: **on**
- API: **5.9.106.214:1317**

---

- Moniker: **Pro-Nodes_RPC**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **135.181.73.170:26857**
- TxIndex: **on**

---

- Moniker: **BRAND-gravity-relayer**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **213.239.213.142:14257**
- TxIndex: **on**

---

- Moniker: **vmi880266.contaboserver.net**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **194.147.58.224:26657**
- TxIndex: **on**

---

- Moniker: **BRAND-gravity-relayer**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **213.239.213.142:14257**
- TxIndex: **on**

---

- Moniker: **amhost-2**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **93.186.201.171:26657**
- TxIndex: **on**

---

- Moniker: **qubelabs**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **195.201.202.39:26657**
- TxIndex: **on**

---

- Moniker: **qubelabs**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **195.201.202.39:26657**
- TxIndex: **on**

---

- Moniker: **maxfoton**
- Chain ID: ****
- Latest block: **8934157**
- RPC: **46.8.220.127:26657**
- TxIndex: **off**

---

- Moniker: **amhost | seed node 01**
- Chain ID: ****
- Latest block: **8923585**
- RPC: **95.211.103.175:26657**
- TxIndex: **off**

---

- Moniker: **amhost-2**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **51.79.20.76:26657**
- TxIndex: **on**

---

- Moniker: **test**
- Chain ID: ****
- Latest block: **8933863**
- RPC: **65.19.136.133:26657**
- TxIndex: **on**
- API: **65.19.136.133:1317**

---

- Moniker: **test**
- Chain ID: ****
- Latest block: **8933863**
- RPC: **65.19.136.133:26657**
- TxIndex: **on**
- API: **65.19.136.133:1317**

---

- Moniker: **Teku**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **65.108.109.103:11657**
- TxIndex: **off**

---

- Moniker: **ramuchi.tech**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **142.132.202.86:36657**
- TxIndex: **on**
- API: **142.132.202.86:1322**

---

- Moniker: **Staketab-Snap**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **65.21.91.99:26777**
- TxIndex: **off**
- API: **65.21.91.99:1317**

---

- Moniker: **mymoniker**
- Chain ID: ****
- Latest block: **8934157**
- RPC: **172.104.202.149:26657**
- TxIndex: **on**

---

- Moniker: **blockscape-seed**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **18.198.207.118:26657**
- TxIndex: **on**
- API: **18.198.207.118:1317**

---

- Moniker: **tmp98iuj**
- Chain ID: ****
- Latest block: **8934181**
- RPC: **93.159.130.6:36657**
- TxIndex: **on**

---

</details>

<details>
<summary>Kava</summary>

- Moniker: **kava-yieldmos-2**
- Chain ID: ****
- Latest block: **7017096**
- RPC: **65.108.235.36:23657**
- TxIndex: **on**

---

- Moniker: **kava-yieldmos-2**
- Chain ID: ****
- Latest block: **7017096**
- RPC: **65.108.235.36:23657**
- TxIndex: **on**

---

- Moniker: **lets_node**
- Chain ID: ****
- Latest block: **7017096**
- RPC: **142.132.150.14:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Quicksilver</summary>

- Moniker: **BRAND-quicksilver-relayer**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **85.10.197.58:11157**
- TxIndex: **on**

---

- Moniker: **Colinka**
- Chain ID: ****
- Latest block: **2149250**
- RPC: **85.10.198.171:26602**
- TxIndex: **on**

---

- Moniker: **Fort**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **161.97.82.203:26257**
- TxIndex: **on**
- API: **161.97.82.203:1317**

---

- Moniker: **Fort**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **161.97.82.203:26257**
- TxIndex: **on**
- API: **161.97.82.203:1317**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **65.109.28.177:28657**
- TxIndex: **off**

---

- Moniker: **nkbblocks**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **46.4.121.72:26657**
- TxIndex: **on**

---

- Moniker: **Staketab-snap**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **65.108.195.29:31127**
- TxIndex: **off**
- API: **65.108.195.29:1318**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **4275725**
- RPC: **174.138.180.190:61157**
- TxIndex: **on**
- API: **174.138.180.190:1317**

---

</details>

<details>
<summary>Bitcanna</summary>

- Moniker: **L0vd.com | RPC**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **65.109.33.48:17657**
- TxIndex: **on**

---

- Moniker: **STAVR-RPC**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **88.99.164.158:21327**
- TxIndex: **on**

---

- Moniker: **bitcanna**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **65.108.131.190:21957**
- TxIndex: **on**

---

- Moniker: **Kannabia Seeds**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **65.108.43.171:26657**
- TxIndex: **on**

---

- Moniker: **Moniker**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **95.216.242.82:36657**
- TxIndex: **on**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **65.108.142.81:26683**
- TxIndex: **on**
- API: **65.108.142.81:1321**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **93.115.25.15:26657**
- TxIndex: **on**

---

- Moniker: **New_peer**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **161.97.150.65:26657**
- TxIndex: **on**

---

- Moniker: **Paranorm**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **193.34.144.156:26657**
- TxIndex: **on**

---

- Moniker: **New_peer**
- Chain ID: ****
- Latest block: **10913536**
- RPC: **154.12.232.8:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>CosmosHub</summary>

- Moniker: **uGaenn-cosmos-relayer**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **95.216.16.205:14957**
- TxIndex: **on**

---

- Moniker: **cbd8h63je8haklvb9770**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **74.118.143.189:26657**
- TxIndex: **on**

---

- Moniker: **gaia**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **138.201.220.51:26677**
- TxIndex: **on**
- API: **138.201.220.51:1327**

---

- Moniker: **cbd91sc80fg04ahd7rmg**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **204.16.241.207:26657**
- TxIndex: **on**

---

- Moniker: **w3coins-cosmos-main**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **85.10.197.58:14957**
- TxIndex: **on**

---

- Moniker: **aws-sgp-g3-atom**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **18.138.176.63:26657**
- TxIndex: **on**
- API: **18.138.176.63:1317**

---

- Moniker: **LiveRaveN**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **142.132.199.236:26657**
- TxIndex: **off**
- API: **142.132.199.236:1317**

---

- Moniker: **98hntjbunjvs**
- Chain ID: ****
- Latest block: **17542358**
- RPC: **93.159.130.8:26657**
- TxIndex: **on**

---

- Moniker: **234tgrfdwd**
- Chain ID: ****
- Latest block: **17152136**
- RPC: **168.119.5.27:26657**
- TxIndex: **on**

---

- Moniker: **harry-smith**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **65.21.94.45:47757**
- TxIndex: **on**
- API: **65.21.94.45:1321**

---

- Moniker: **s3**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **178.18.249.59:26657**
- TxIndex: **on**

---

- Moniker: **o21bsao91**
- Chain ID: ****
- Latest block: **17542369**
- RPC: **95.216.114.244:26657**
- TxIndex: **off**

---

</details>

<details>
<summary>Evmos</summary>

- Moniker: **bricks_evmos_2**
- Chain ID: ****
- Latest block: **16659436**
- RPC: **65.109.84.24:26657**
- TxIndex: **on**
- API: **65.109.84.24:1317**

---

- Moniker: **BRAND-evmos-relayer**
- Chain ID: ****
- Latest block: **16659436**
- RPC: **213.239.213.142:13457**
- TxIndex: **on**

---

- Moniker: **SWU**
- Chain ID: ****
- Latest block: **16659436**
- RPC: **5.9.87.216:45557**
- TxIndex: **on**
- API: **5.9.87.216:1311**

---

- Moniker: **mefn1**
- Chain ID: ****
- Latest block: **16659436**
- RPC: **62.171.184.44:26657**
- TxIndex: **on**
- API: **62.171.184.44:1317**

---

- Moniker: **bhcreovh**
- Chain ID: ****
- Latest block: **16659436**
- RPC: **135.125.189.180:26657**
- TxIndex: **on**
- API: **135.125.189.180:1317**

---

</details>

<details>
<summary>Fetch</summary>

- Moniker: **yieldmos-fetch**
- Chain ID: ****
- Latest block: **13469452**
- RPC: **65.109.35.50:14657**
- TxIndex: **on**

---

- Moniker: **yieldmos-fetch**
- Chain ID: ****
- Latest block: **13469452**
- RPC: **65.109.35.50:14657**
- TxIndex: **on**

---

- Moniker: **BRAND-fetch-relayer**
- Chain ID: ****
- Latest block: **13469452**
- RPC: **5.9.99.172:15257**
- TxIndex: **on**

---

- Moniker: **Outlier Ventures**
- Chain ID: ****
- Latest block: **13469452**
- RPC: **95.216.159.232:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Osmosis</summary>

- Moniker: **cbmk93o0ivsupsnju960**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **141.98.217.102:26657**
- TxIndex: **on**

---

- Moniker: **cbmk93o0ivsupsnju960**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **141.98.217.102:26657**
- TxIndex: **on**

---

- Moniker: **cbmk8mg0ivsupsnju950**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **141.98.219.104:26657**
- TxIndex: **on**

---

- Moniker: **cbmk8mg0ivsupsnju950**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **141.98.219.104:26657**
- TxIndex: **on**

---

- Moniker: **rebot-bada**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.108.204.56:26657**
- TxIndex: **on**
- API: **65.108.204.56:1317**

---

- Moniker: **BRAND-osmosis-relayer**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **85.10.197.58:12557**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **66.172.36.139:36657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **66.172.36.140:36657**
- TxIndex: **on**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.109.93.152:38657**
- TxIndex: **on**

---

- Moniker: **test**
- Chain ID: ****
- Latest block: **6246000**
- RPC: **23.82.88.133:26657**
- TxIndex: **on**

---

- Moniker: **ramuchi.tech**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **142.132.202.86:46657**
- TxIndex: **on**
- API: **142.132.202.86:1320**

---

- Moniker: **ramuchi.tech**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **142.132.202.86:46657**
- TxIndex: **on**
- API: **142.132.202.86:1322**

---

- Moniker: **LiveRaveN**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **142.132.199.236:28657**
- TxIndex: **on**
- API: **142.132.199.236:1317**

---

- Moniker: **osmosis**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.109.20.216:26657**
- TxIndex: **on**
- API: **65.109.20.216:1317**

---

- Moniker: **osmosis**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.109.20.216:26657**
- TxIndex: **on**
- API: **65.109.20.216:1317**

---

- Moniker: **Staketab-snap**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.21.91.99:16957**
- TxIndex: **off**
- API: **65.21.91.99:1319**

---

- Moniker: **osmosis**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.109.20.216:26657**
- TxIndex: **on**
- API: **65.109.20.216:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **12023866**
- RPC: **100.26.5.185:26657**
- TxIndex: **on**
- API: **100.26.5.185:1317**

---

- Moniker: **xxxxxxxxxxxxxxxxxxx**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **65.108.142.81:26680**
- TxIndex: **on**
- API: **65.108.142.81:1321**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **176.9.158.219:41057**
- TxIndex: **on**

---

- Moniker: **osmosis-archive-osmosis-1-a**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **15.164.13.43:26657**
- TxIndex: **on**
- API: **15.164.13.43:1317**

---

- Moniker: **osmosis-archive-osmosis-1-a**
- Chain ID: ****
- Latest block: **12024646**
- RPC: **15.164.13.43:26657**
- TxIndex: **on**
- API: **15.164.13.43:1317**

---

</details>

<details>
<summary>Persistence</summary>

- Moniker: **HS**
- Chain ID: ****
- Latest block: **13724549**
- RPC: **38.242.142.199:46657**
- TxIndex: **off**
- API: **38.242.142.199:1318**

---

- Moniker: **yieldmos-xprt**
- Chain ID: ****
- Latest block: **13724549**
- RPC: **65.108.235.36:27657**
- TxIndex: **on**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **13724549**
- RPC: **65.109.28.177:26227**
- TxIndex: **on**

---

- Moniker: **VaultRPC**
- Chain ID: ****
- Latest block: **12716803**
- RPC: **51.81.16.189:26657**
- TxIndex: **on**
- API: **51.81.16.189:1317**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **13724549**
- RPC: **65.109.28.177:26227**
- TxIndex: **on**

---

- Moniker: **PBR**
- Chain ID: ****
- Latest block: **13724549**
- RPC: **135.181.183.212:25657**
- TxIndex: **on**

---

- Moniker: **Snap**
- Chain ID: ****
- Latest block: **13724549**
- RPC: **193.34.144.156:25657**
- TxIndex: **on**

---

</details>

<details>
<summary>Canto</summary>

- Moniker: **canto**
- Chain ID: ****
- Latest block: **6541203**
- RPC: **138.201.85.176:26677**
- TxIndex: **on**

---

- Moniker: **canto**
- Chain ID: ****
- Latest block: **6541203**
- RPC: **138.201.85.176:26677**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **6541203**
- RPC: **65.108.141.109:16657**
- TxIndex: **on**
- API: **65.108.141.109:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **6541204**
- RPC: **66.172.36.136:51657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **6541204**
- RPC: **66.172.36.134:51657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **6541203**
- RPC: **65.108.75.107:16657**
- TxIndex: **on**

---

- Moniker: **moodman**
- Chain ID: ****
- Latest block: **6541203**
- RPC: **65.109.65.210:29657**
- TxIndex: **off**

---

- Moniker: **74891e7b0a7c**
- Chain ID: ****
- Latest block: **6541203**
- RPC: **142.93.47.206:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Crescent</summary>

- Moniker: **yieldmos**
- Chain ID: ****
- Latest block: **9133520**
- RPC: **65.109.35.50:15657**
- TxIndex: **on**

---

- Moniker: **yieldmos**
- Chain ID: ****
- Latest block: **9133520**
- RPC: **65.109.35.50:15657**
- TxIndex: **on**

---

- Moniker: **BRAND-crescent-relayer**
- Chain ID: ****
- Latest block: **9133520**
- RPC: **5.9.99.172:14557**
- TxIndex: **on**

---

</details>

<details>
<summary>Lum</summary>

- Moniker: **sentry-1**
- Chain ID: ****
- Latest block: **9875407**
- RPC: **51.15.142.113:26657**
- TxIndex: **off**

---

- Moniker: **sentry-0**
- Chain ID: ****
- Latest block: **9875407**
- RPC: **163.172.173.147:26657**
- TxIndex: **off**

---

- Moniker: **public-node-0**
- Chain ID: ****
- Latest block: **9875406**
- RPC: **51.158.111.136:26657**
- TxIndex: **on**
- API: **51.158.111.136:1317**

---

- Moniker: **public-node-1**
- Chain ID: ****
- Latest block: **9875407**
- RPC: **212.47.250.217:26657**
- TxIndex: **on**
- API: **212.47.250.217:1317**

---

- Moniker: **ELZ-02**
- Chain ID: ****
- Latest block: **9875407**
- RPC: **176.57.150.227:26657**
- TxIndex: **off**

---

</details>

<details>
<summary>Sifchain</summary>

- Moniker: **yieldmos-sif**
- Chain ID: ****
- Latest block: **13516121**
- RPC: **65.109.35.50:10657**
- TxIndex: **on**

---

- Moniker: **vchain**
- Chain ID: ****
- Latest block: **14385451**
- RPC: **188.166.241.167:26657**
- TxIndex: **off**
- API: **188.166.241.167:1317**

---

- Moniker: **vchain**
- Chain ID: ****
- Latest block: **14385451**
- RPC: **188.166.241.167:26657**
- TxIndex: **off**
- API: **188.166.241.167:1317**

---

</details>

<details>
<summary>Stargaze</summary>

- Moniker: **yieldmos-stars**
- Chain ID: ****
- Latest block: **10589573**
- RPC: **65.108.235.36:17657**
- TxIndex: **on**

---

- Moniker: **silent**
- Chain ID: ****
- Latest block: **10589573**
- RPC: **65.108.141.109:8657**
- TxIndex: **on**
- API: **65.108.141.109:1317**

---

- Moniker: **BRAND-stargaze-relayer**
- Chain ID: ****
- Latest block: **10589573**
- RPC: **5.9.99.172:13757**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10589573**
- RPC: **65.108.75.107:8657**
- TxIndex: **on**

---

- Moniker: **ramuchi.tech**
- Chain ID: ****
- Latest block: **10589573**
- RPC: **142.132.202.86:26657**
- TxIndex: **on**
- API: **142.132.202.86:1325**

---

</details>

<!-- END_MAINNET -->
</details>

<details>
  <summary>Testnets</summary>
  
  Simply browse the testnets section to find the public endpoints you need for test networks.
<!-- START_TESTNET -->
<details>
<summary>Noria</summary>

- Moniker: **L0vd.com**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **65.109.70.45:12657**
- TxIndex: **off**

---

- Moniker: **4329345-sdagasdg-3459-gntr**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **65.108.105.48:22157**
- TxIndex: **on**

---

- Moniker: **WellNode**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **65.108.211.139:26657**
- TxIndex: **off**

---

- Moniker: **Sr20de**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **46.17.250.108:61357**
- TxIndex: **off**
- API: **46.17.250.108:1317**

---

- Moniker: **Moonbridge**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **195.3.221.16:12057**
- TxIndex: **off**
- API: **195.3.221.16:1317**

---

- Moniker: **lesnik_utsa**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **65.108.206.118:61657**
- TxIndex: **on**

---

- Moniker: **av_noria_1**
- Chain ID: ****
- Latest block: **3283645**
- RPC: **102.130.121.211:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Andromeda</summary>

- Moniker: **[NODERS]TEAM**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **65.108.227.112:14657**
- TxIndex: **on**

---

- Moniker: **Munris**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **65.21.170.3:32657**
- TxIndex: **off**

---

- Moniker: **RAMZES**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **65.108.199.120:61357**
- TxIndex: **on**
- API: **65.108.199.120:1317**

---

- Moniker: **cardex**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **161.97.152.157:27657**
- TxIndex: **on**

---

- Moniker: **AviaDoc_by_AviaOne**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **194.163.174.231:26677**
- TxIndex: **off**

---

- Moniker: **F5Nodes**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **78.46.103.246:60957**
- TxIndex: **off**

---

- Moniker: **Moonbridge**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **195.3.221.16:12757**
- TxIndex: **off**
- API: **195.3.221.16:1317**

---

- Moniker: **landeros**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **213.239.207.175:42657**
- TxIndex: **on**

---

- Moniker: **ksalab**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **65.109.88.254:40657**
- TxIndex: **on**
- API: **65.109.88.254:1317**

---

- Moniker: **a4951031-6d09-5ee9-9e28-e063741b480d**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **142.132.209.236:21257**
- TxIndex: **on**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **65.21.133.114:56657**
- TxIndex: **on**
- API: **65.21.133.114:1320**

---

- Moniker: **andromeda**
- Chain ID: ****
- Latest block: **3511702**
- RPC: **194.34.232.224:56657**
- TxIndex: **off**

---

</details>

<details>
<summary>Aura</summary>

- Moniker: **ac8dca3201fa9eabf641dfa662fb12cb**
- Chain ID: ****
- Latest block: **7175000**
- RPC: **89.117.56.126:25057**
- TxIndex: **off**

---

- Moniker: **OnBlock Ventures**
- Chain ID: ****
- Latest block: **7175000**
- RPC: **89.58.59.10:26657**
- TxIndex: **on**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **7175000**
- RPC: **65.108.195.29:36127**
- TxIndex: **off**
- API: **65.108.195.29:1318**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **7175000**
- RPC: **65.108.233.44:21657**
- TxIndex: **on**

---

</details>

<details>
<summary>Babylon</summary>

- Moniker: **B-Harvest**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **141.95.97.28:15557**
- TxIndex: **on**

---

- Moniker: **B-Harvest**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **141.95.97.28:15557**
- TxIndex: **on**

---

- Moniker: **RPC**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **65.108.194.111:32657**
- TxIndex: **on**

---

- Moniker: **[NODERS]TEAM**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **49.12.84.248:16657**
- TxIndex: **on**

---

- Moniker: **Moonbridge**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **195.3.221.16:12857**
- TxIndex: **off**
- API: **195.3.221.16:1317**

---

- Moniker: **ksalab**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **65.109.88.254:38657**
- TxIndex: **on**
- API: **65.109.88.254:1317**

---

- Moniker: **UTSA_guide**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **65.108.206.118:61457**
- TxIndex: **on**

---

- Moniker: **babylon**
- Chain ID: ****
- Latest block: **1222495**
- RPC: **3.18.176.128:26657**
- TxIndex: **on**
- API: **3.18.176.128:1317**

---

</details>

<details>
<summary>Evmos</summary>

- Moniker: **alex**
- Chain ID: ****
- Latest block: **18129160**
- RPC: **65.108.238.61:22657**
- TxIndex: **on**
- API: **65.108.238.61:1307**

---

- Moniker: **arctelephant**
- Chain ID: ****
- Latest block: **18129160**
- RPC: **142.132.198.157:26657**
- TxIndex: **on**
- API: **142.132.198.157:1317**

---

- Moniker: **arctelephant**
- Chain ID: ****
- Latest block: **18129160**
- RPC: **142.132.198.157:26657**
- TxIndex: **on**
- API: **142.132.198.157:1317**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **18129160**
- RPC: **65.108.79.246:26665**
- TxIndex: **on**

---

</details>

<details>
<summary>Injective</summary>

- Moniker: **injective**
- Chain ID: ****
- Latest block: **17505330**
- RPC: **146.59.252.210:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **17505330**
- RPC: **146.59.252.210:26657**
- TxIndex: **on**

---

- Moniker: **injective**
- Chain ID: ****
- Latest block: **17407887**
- RPC: **148.113.153.79:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Jackal</summary>

- Moniker: **MantiCore**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **65.108.124.219:41657**
- TxIndex: **off**

---

- Moniker: **moodman**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **144.126.140.252:29657**
- TxIndex: **off**

---

- Moniker: **cyberG**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **51.89.232.234:27687**
- TxIndex: **on**
- API: **51.89.232.234:1317**

---

- Moniker: **STAVR-Service**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **65.108.230.113:19127**
- TxIndex: **on**

---

- Moniker: **LiluPunk**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **65.109.93.152:31657**
- TxIndex: **on**

---

- Moniker: **landeros | StakeUp**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **213.239.207.175:48657**
- TxIndex: **off**

---

- Moniker: **web34ever**
- Chain ID: ****
- Latest block: **4919272**
- RPC: **95.217.207.236:27687**
- TxIndex: **on**
- API: **95.217.207.236:1317**

---

</details>

<details>
<summary>Lava</summary>

- Moniker: **mynode**
- Chain ID: ****
- Latest block: **350453**
- RPC: **51.159.100.162:26657**
- TxIndex: **on**

---

- Moniker: **mynode**
- Chain ID: ****
- Latest block: **27**
- RPC: **85.239.237.85:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Neutron</summary>

- Moniker: **pion-1-banana**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **2604:a880:cad:d0::e65:d001:26657**
- TxIndex: **on**
- API: **2604:a880:cad:d0::e65:d001:1317**

---

- Moniker: **pion-1-banana**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **2604:a880:cad:d0::e65:d001:26657**
- TxIndex: **on**
- API: **2604:a880:cad:d0::e65:d001:1317**

---

- Moniker: **pion-1-sentry-1**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **2604:a880:cad:d0::f11:1:26657**
- TxIndex: **on**
- API: **2604:a880:cad:d0::f11:1:1317**

---

- Moniker: **pion-1-apple**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **2604:a880:cad:d0::d0a:5001:26657**
- TxIndex: **on**
- API: **2604:a880:cad:d0::d0a:5001:1317**

---

- Moniker: **B-Harvest**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **162.19.170.233:14457**
- TxIndex: **on**

---

- Moniker: **B-Harvest**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **162.19.170.233:14457**
- TxIndex: **on**

---

- Moniker: **pion-1-cherry**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **2604:a880:cad:d0::d05:a001:26657**
- TxIndex: **on**
- API: **2604:a880:cad:d0::d05:a001:1317**

---

- Moniker: **pion-1-apple**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **165.227.41.163:26657**
- TxIndex: **on**
- API: **165.227.41.163:1317**

---

- Moniker: **pion-1-sentry-1**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **146.190.242.140:26657**
- TxIndex: **on**
- API: **146.190.242.140:1317**

---

- Moniker: **pion-1-banana**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **137.184.168.202:26657**
- TxIndex: **on**
- API: **137.184.168.202:1317**

---

- Moniker: **pion-1-cherry**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **134.122.45.252:26657**
- TxIndex: **on**
- API: **134.122.45.252:1317**

---

- Moniker: **pion-1-cherry**
- Chain ID: ****
- Latest block: **6579461**
- RPC: **134.122.45.252:26657**
- TxIndex: **on**
- API: **134.122.45.252:1317**

---

</details>

<details>
<summary>Axelar</summary>

- Moniker: **JuliVal**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.108.69.42:26657**
- TxIndex: **on**
- API: **65.108.69.42:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.108.69.56:56657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **95.217.198.60:26657**
- TxIndex: **on**
- API: **95.217.198.60:1317**

---

- Moniker: **haciyatmaz**
- Chain ID: ****
- Latest block: **7127971**
- RPC: **138.201.204.5:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **163.172.148.33:26657**
- TxIndex: **on**
- API: **163.172.148.33:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **84.201.159.4:26657**
- TxIndex: **on**
- API: **84.201.159.4:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **84.201.159.4:26657**
- TxIndex: **on**
- API: **84.201.159.4:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.108.201.189:26657**
- TxIndex: **on**
- API: **65.108.201.189:1317**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **51.89.155.177:26657**
- TxIndex: **on**
- API: **51.89.155.177:1317**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **51.89.155.177:26657**
- TxIndex: **on**
- API: **51.89.155.177:1317**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **51.89.155.177:26657**
- TxIndex: **on**
- API: **51.89.155.177:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **38.146.3.230:15157**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **3.137.103.123:26657**
- TxIndex: **on**
- API: **3.137.103.123:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **3.17.83.193:26657**
- TxIndex: **on**
- API: **3.17.83.193:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **3.15.107.202:26657**
- TxIndex: **on**
- API: **3.15.107.202:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **3.139.119.119:26657**
- TxIndex: **on**
- API: **3.139.119.119:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.109.23.114:15157**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **65.21.226.198:26657**
- TxIndex: **on**

---

- Moniker: **ip-172-31-42-49**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **18.188.23.105:26657**
- TxIndex: **on**
- API: **18.188.23.105:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **10198520**
- RPC: **8.219.219.29:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>Dymension</summary>

- Moniker: **b-t-dym-s0**
- Chain ID: ****
- Latest block: **3736790**
- RPC: **141.95.97.28:15257**
- TxIndex: **off**

---

- Moniker: **1d71c8581048d7ae**
- Chain ID: ****
- Latest block: **3736790**
- RPC: **89.117.56.126:24757**
- TxIndex: **off**

---

- Moniker: **dymension_testnet**
- Chain ID: ****
- Latest block: **3736790**
- RPC: **136.243.88.91:3241**
- TxIndex: **on**

---

- Moniker: **cryptech**
- Chain ID: ****
- Latest block: **3736790**
- RPC: **185.144.99.22:26657**
- TxIndex: **on**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **3736790**
- RPC: **141.94.138.48:26674**
- TxIndex: **on**

---

- Moniker: **bro_n_bro**
- Chain ID: ****
- Latest block: **3736790**
- RPC: **195.201.195.61:27657**
- TxIndex: **on**

---

</details>

<details>
<summary>EVMOS</summary>

- Moniker: **Monika**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **66.94.103.61:17057**
- TxIndex: **on**

---

- Moniker: **alex**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **65.108.238.61:22657**
- TxIndex: **on**
- API: **65.108.238.61:1307**

---

- Moniker: **Pro-Nodes75**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **148.251.8.22:26857**
- TxIndex: **on**

---

- Moniker: **qubelabs**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **157.90.208.234:26677**
- TxIndex: **on**

---

- Moniker: **arctelephant**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **142.132.198.157:26657**
- TxIndex: **on**
- API: **142.132.198.157:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **65.108.75.107:27657**
- TxIndex: **on**

---

- Moniker: **STAVR**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **65.108.230.113:60757**
- TxIndex: **on**

---

- Moniker: **evmost-testnet**
- Chain ID: ****
- Latest block: **18034000**
- RPC: **5.9.107.249:36657**
- TxIndex: **on**
- API: **5.9.107.249:1317**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **18129156**
- RPC: **65.108.79.246:26665**
- TxIndex: **on**

---

</details>

<details>
<summary>Empower</summary>

- Moniker: **0d6e3b789658bf7f**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **89.117.56.126:24357**
- TxIndex: **off**

---

- Moniker: **AMS**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **65.108.72.233:42657**
- TxIndex: **on**
- API: **65.108.72.233:1317**

---

- Moniker: **nodesll-rpc**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **144.76.99.105:26657**
- TxIndex: **on**
- API: **144.76.99.105:1317**

---

- Moniker: **nodesll-rpc**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **144.76.99.105:26657**
- TxIndex: **on**
- API: **144.76.99.105:1317**

---

- Moniker: **Sr20de**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **46.17.250.108:26657**
- TxIndex: **on**
- API: **46.17.250.108:1327**

---

- Moniker: **Moonbridge**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **195.3.221.16:12657**
- TxIndex: **off**
- API: **195.3.221.16:1317**

---

- Moniker: **ksalab**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **65.109.88.254:44657**
- TxIndex: **on**
- API: **65.109.88.254:1317**

---

- Moniker: **Pandora**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **164.68.119.233:36657**
- TxIndex: **on**

---

- Moniker: **Pandora**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **164.68.119.233:36657**
- TxIndex: **on**

---

- Moniker: **empowerchain**
- Chain ID: ****
- Latest block: **2275012**
- RPC: **185.196.20.192:26657**
- TxIndex: **off**

---

</details>

<details>
<summary>Ojo</summary>

- Moniker: **ojo-devnet**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **138.201.85.176:26697**
- TxIndex: **on**

---

- Moniker: **x0ser**
- Chain ID: ****
- Latest block: **3518577**
- RPC: **178.159.5.187:60657**
- TxIndex: **off**

---

- Moniker: **alex**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **65.108.238.61:20657**
- TxIndex: **on**
- API: **65.108.238.61:1307**

---

- Moniker: **Munris**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **65.21.170.3:38657**
- TxIndex: **off**

---

- Moniker: **Firstcome**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **95.217.225.212:36657**
- TxIndex: **on**

---

- Moniker: **L0vd.com**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **65.109.70.45:16657**
- TxIndex: **off**

---

- Moniker: **RAMZES**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **65.108.199.120:54657**
- TxIndex: **on**
- API: **65.108.199.120:1327**

---

- Moniker: **CosmoBook**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **178.18.254.211:11657**
- TxIndex: **off**

---

- Moniker: **ojo_testnet**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **136.243.88.91:7331**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **88.198.32.17:39657**
- TxIndex: **on**

---

- Moniker: **maximyourich**
- Chain ID: ****
- Latest block: **0**
- RPC: **168.119.66.95:34657**
- TxIndex: **on**

---

- Moniker: **AlxVoy**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **65.109.93.152:33657**
- TxIndex: **on**

---

- Moniker: **landeros**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **213.239.207.175:47657**
- TxIndex: **off**

---

- Moniker: **moodman**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **173.212.222.167:30657**
- TxIndex: **off**

---

- Moniker: **Galaxynode**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **95.217.224.252:19657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **142.132.209.236:21657**
- TxIndex: **on**

---

- Moniker: **Staketab**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **78.46.99.50:22657**
- TxIndex: **on**
- API: **78.46.99.50:1319**

---

- Moniker: **Pro-Nodes75**
- Chain ID: ****
- Latest block: **3698944**
- RPC: **142.132.134.112:25357**
- TxIndex: **on**
- API: **142.132.134.112:1327**

---

</details>

<details>
<summary>Quicksilver</summary>

- Moniker: **Firstcome**
- Chain ID: ****
- Latest block: **2802595**
- RPC: **31.220.84.183:19657**
- TxIndex: **off**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **2802595**
- RPC: **65.108.79.246:26674**
- TxIndex: **on**

---

</details>

<details>
<summary>Elys</summary>

- Moniker: **82f47e91-3baa-5648-bb81-8e5fdd97eac0**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **65.108.105.48:22057**
- TxIndex: **on**

---

- Moniker: **vn01.elys.fasthub.io**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **185.169.252.221:26657**
- TxIndex: **off**

---

- Moniker: **Pro-Nodes75**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **168.119.226.107:22357**
- TxIndex: **on**

---

- Moniker: **moniker**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **37.187.154.66:26657**
- TxIndex: **on**

---

- Moniker: **moniker**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **37.187.154.66:26657**
- TxIndex: **on**

---

- Moniker: **moniker**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **198.27.74.140:26657**
- TxIndex: **on**

---

- Moniker: **moniker**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **198.27.74.140:26657**
- TxIndex: **on**

---

- Moniker: **node1**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **95.217.107.96:26157**
- TxIndex: **on**

---

- Moniker: **node-1-st**
- Chain ID: ****
- Latest block: **3422457**
- RPC: **65.108.195.29:23657**
- TxIndex: **off**
- API: **65.108.195.29:1318**

---

- Moniker: **Synergy_Nodes**
- Chain ID: ****
- Latest block: **3422456**
- RPC: **65.109.93.35:57657**
- TxIndex: **on**

---

</details>

<details>
<summary>Kava</summary>

- Moniker: **lets_node**
- Chain ID: ****
- Latest block: **7936267**
- RPC: **148.251.82.149:26657**
- TxIndex: **on**

---

- Moniker: **colossus-testnet1**
- Chain ID: ****
- Latest block: **7936267**
- RPC: **167.235.244.61:26657**
- TxIndex: **on**
- API: **167.235.244.61:1317**

---

</details>

<details>
<summary>Stargaze</summary>

- Moniker: **node1**
- Chain ID: ****
- Latest block: **7330274**
- RPC: **209.159.152.82:26662**
- TxIndex: **on**

---

- Moniker: **node0**
- Chain ID: ****
- Latest block: **7330274**
- RPC: **209.159.152.82:26657**
- TxIndex: **on**

---

- Moniker: **node3**
- Chain ID: ****
- Latest block: **7330274**
- RPC: **208.73.205.226:26662**
- TxIndex: **on**

---

- Moniker: **node2**
- Chain ID: ****
- Latest block: **7330274**
- RPC: **208.73.205.226:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>ZetaChain</summary>

- Moniker: **zig**
- Chain ID: ****
- Latest block: **1797600**
- RPC: **135.181.115.175:26657**
- TxIndex: **on**
- API: **135.181.115.175:1317**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **51.75.90.106:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **5.9.60.44:31461**
- TxIndex: **on**

---

- Moniker: **ProtofireDAO**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **3.233.186.130:26657**
- TxIndex: **on**

---

- Moniker: **HashQuark**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **152.32.150.236:26657**
- TxIndex: **on**
- API: **152.32.150.236:1317**

---

- Moniker: **HashQuark**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **152.32.150.236:26657**
- TxIndex: **on**
- API: **152.32.150.236:1317**

---

- Moniker: **ttp**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **142.132.202.87:26657**
- TxIndex: **on**
- API: **142.132.202.87:1316**

---

- Moniker: **bm-ex44**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **46.4.15.110:26657**
- TxIndex: **on**

---

- Moniker: **foreststaking**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **88.218.226.79:26657**
- TxIndex: **on**
- API: **88.218.226.79:1317**

---

- Moniker: **RockX**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **141.94.214.137:26657**
- TxIndex: **off**
- API: **141.94.214.137:1317**

---

- Moniker: **rocket**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **161.97.107.122:41657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **15.235.160.207:26657**
- TxIndex: **on**

---

- Moniker: **node**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **15.235.160.207:26657**
- TxIndex: **on**

---

- Moniker: **STAKECRAFT**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **65.108.124.57:25657**
- TxIndex: **on**
- API: **65.108.124.57:1317**

---

- Moniker: **Yuriy78**
- Chain ID: ****
- Latest block: **14417**
- RPC: **65.108.66.247:26657**
- TxIndex: **on**

---

- Moniker: **Yuriy78**
- Chain ID: ****
- Latest block: **14417**
- RPC: **65.108.66.247:26657**
- TxIndex: **on**

---

- Moniker: **archive0**
- Chain ID: ****
- Latest block: **5007312**
- RPC: **35.170.251.63:26657**
- TxIndex: **on**

---

- Moniker: **banana**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **91.194.30.204:28657**
- TxIndex: **on**

---

- Moniker: **statesync0-us-east-1**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **52.3.196.71:26657**
- TxIndex: **on**

---

- Moniker: **seed0-us-east-1**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **52.204.29.109:26657**
- TxIndex: **on**

---

- Moniker: **statesync0-ap-southeast-1**
- Chain ID: ****
- Latest block: **2159147**
- RPC: **3.0.80.230:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>CosmosHub</summary>

- Moniker: **B-Harvest**
- Chain ID: ****
- Latest block: **3700383**
- RPC: **162.19.170.233:15457**
- TxIndex: **on**

---

- Moniker: **stakely**
- Chain ID: ****
- Latest block: **3700383**
- RPC: **141.94.138.48:26677**
- TxIndex: **on**

---

</details>

<details>
<summary>Juno</summary>

- Moniker: **ams**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **65.108.44.149:11657**
- TxIndex: **on**

---

- Moniker: **OranG3cluB**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **65.109.88.251:33657**
- TxIndex: **on**

---

- Moniker: **STAVR**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **65.109.92.241:1067**
- TxIndex: **on**

---

- Moniker: **Sr20de**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **46.17.250.108:61157**
- TxIndex: **on**
- API: **46.17.250.108:1317**

---

- Moniker: **daodao_indexer**
- Chain ID: ****
- Latest block: **4539290**
- RPC: **143.198.141.42:26657**
- TxIndex: **off**

---

- Moniker: **testnets-node**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **95.216.101.38:36657**
- TxIndex: **on**

---

- Moniker: **testnets-node**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **95.216.101.38:36657**
- TxIndex: **on**

---

- Moniker: **3b652bff-9838-5089-9329-c36d8f3deceb**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **142.132.209.236:12657**
- TxIndex: **on**

---

- Moniker: **Stakely.io**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **65.108.79.246:26668**
- TxIndex: **on**

---

- Moniker: **Stake&Relax Validator**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **194.61.28.217:26657**
- TxIndex: **on**

---

- Moniker: **Stake&Relax Validator**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **194.61.28.217:26657**
- TxIndex: **on**

---

- Moniker: **Stake&Relax Validator**
- Chain ID: ****
- Latest block: **4539289**
- RPC: **194.61.28.217:26657**
- TxIndex: **on**

---

</details>

<details>
<summary>OKP4</summary>

- Moniker: **Munris**
- Chain ID: ****
- Latest block: **4717170**
- RPC: **65.21.170.3:42657**
- TxIndex: **on**

---

- Moniker: **Vagif**
- Chain ID: ****
- Latest block: **4717170**
- RPC: **94.130.137.122:29657**
- TxIndex: **off**

---

- Moniker: **Enigma**
- Chain ID: ****
- Latest block: **4717170**
- RPC: **136.243.88.91:6041**
- TxIndex: **off**

---

- Moniker: **cryptech**
- Chain ID: ****
- Latest block: **4717170**
- RPC: **185.144.99.16:26657**
- TxIndex: **off**

---

- Moniker: **okp4**
- Chain ID: ****
- Latest block: **4717170**
- RPC: **65.108.131.190:23457**
- TxIndex: **off**

---

- Moniker: **HSS**
- Chain ID: ****
- Latest block: **4717170**
- RPC: **167.235.21.165:46657**
- TxIndex: **off**

---

</details>

<details>
<summary>Osmosis</summary>

- Moniker: **ams**
- Chain ID: ****
- Latest block: **3299851**
- RPC: **65.108.72.233:46657**
- TxIndex: **on**
- API: **65.108.72.233:1317**

---

- Moniker: **secret-osmo**
- Chain ID: ****
- Latest block: **1661929**
- RPC: **23.105.148.5:26657**
- TxIndex: **on**
- API: **23.105.148.5:1317**

---

- Moniker: **testnets-node**
- Chain ID: ****
- Latest block: **3299851**
- RPC: **95.216.101.38:26657**
- TxIndex: **on**

---

- Moniker: **rpc-integrators-osmo-test-5-fra1-0**
- Chain ID: ****
- Latest block: **3299851**
- RPC: **142.93.175.50:26657**
- TxIndex: **on**
- API: **142.93.175.50:1317**

---

- Moniker: **osmosis**
- Chain ID: ****
- Latest block: **3299851**
- RPC: **38.146.3.230:12557**
- TxIndex: **on**

---

- Moniker: **cudoventures.com**
- Chain ID: ****
- Latest block: **3299851**
- RPC: **35.193.49.115:26657**
- TxIndex: **on**
- API: **35.193.49.115:1317**

---

</details>

<!-- END_TESTNET -->
</details>

## Contributing

If you have suggestions for improvement or updates to the data, please create an Issue or Pull Request.

## Contact Us

If you have any questions or suggestions, please contact us through [issues](https://github.com/nodersteam/noderslabs/issues) or via email at `tech@noders.team`
